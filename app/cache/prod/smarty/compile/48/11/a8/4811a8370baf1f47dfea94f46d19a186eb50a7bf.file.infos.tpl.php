<?php /* Smarty version Smarty-3.1.19, created on 2018-05-09 14:37:09
         compiled from "C:\xampp\htdocs\sexilima\modules\ps_wirepayment\views\templates\hook\infos.tpl" */ ?>
<?php /*%%SmartyHeaderCode:10508120555af34de5807a88-26415434%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4811a8370baf1f47dfea94f46d19a186eb50a7bf' => 
    array (
      0 => 'C:\\xampp\\htdocs\\sexilima\\modules\\ps_wirepayment\\views\\templates\\hook\\infos.tpl',
      1 => 1524936026,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10508120555af34de5807a88-26415434',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5af34de5846292_68752006',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5af34de5846292_68752006')) {function content_5af34de5846292_68752006($_smarty_tpl) {?>

<div class="alert alert-info">
<img src="../modules/ps_wirepayment/logo.png" style="float:left; margin-right:15px;" height="60">
<p><strong><?php echo smartyTranslate(array('s'=>"This module allows you to accept secure payments by bank wire.",'d'=>'Modules.Wirepayment.Admin'),$_smarty_tpl);?>
</strong></p>
<p><?php echo smartyTranslate(array('s'=>"If the client chooses to pay by bank wire, the order status will change to 'Waiting for Payment'.",'d'=>'Modules.Wirepayment.Admin'),$_smarty_tpl);?>
</p>
<p><?php echo smartyTranslate(array('s'=>"That said, you must manually confirm the order upon receiving the bank wire.",'d'=>'Modules.Wirepayment.Admin'),$_smarty_tpl);?>
</p>
</div>
<?php }} ?>
