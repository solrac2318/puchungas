<?php /* Smarty version Smarty-3.1.19, created on 2018-05-18 16:19:44
         compiled from "modules\ageverification\views\templates\front\av.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7148929515aff4370204740-50231910%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '47c12811a2d0d77769075a3bac783dd6e52f234d' => 
    array (
      0 => 'modules\\ageverification\\views\\templates\\front\\av.tpl',
      1 => 1526678291,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7148929515aff4370204740-50231910',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'ps_version' => 0,
    'color_r' => 0,
    'color_g' => 0,
    'color_b' => 0,
    'opacity' => 0,
    'bg_color' => 0,
    'font_color' => 0,
    'selected_bg' => 0,
    'selected_color' => 0,
    'header_font' => 0,
    'header_size' => 0,
    'header_mobile' => 0,
    'header_size_mobile' => 0,
    'content_font' => 0,
    'content_size' => 0,
    'content_mobile' => 0,
    'content_size_mobile' => 0,
    'mode' => 0,
    'age' => 0,
    'title' => 0,
    'text' => 0,
    'type' => 0,
    'i' => 0,
    'age_text' => 0,
    'button_text' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5aff43703bde30_82186784',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5aff43703bde30_82186784')) {function content_5aff43703bde30_82186784($_smarty_tpl) {?><style>
    <?php if ($_smarty_tpl->tpl_vars['ps_version']->value!="1.6") {?>
        .remodal-av .dropdown-toggle:after {
            display: none;
        }
    <?php }?>

     .remodal-overlay {
         background: rgba(<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['color_r']->value, ENT_QUOTES, 'UTF-8');?>
, <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['color_g']->value, ENT_QUOTES, 'UTF-8');?>
, <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['color_b']->value, ENT_QUOTES, 'UTF-8');?>
, <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['opacity']->value, ENT_QUOTES, 'UTF-8');?>
) !important;
     }

    .remodal-av {
        background: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['bg_color']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
    }

    <?php if ($_smarty_tpl->tpl_vars['font_color']->value) {?>
        .remodal-av h3, .remodal-av p {
            color: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['font_color']->value, ENT_QUOTES, 'UTF-8');?>
;
        }
    <?php }?>

     <?php if ($_smarty_tpl->tpl_vars['selected_bg']->value||$_smarty_tpl->tpl_vars['selected_color']->value) {?>
        .remodal-av .dropdown-menu > li > a:hover, .remodal-av .dropdown-menu > li > a:focus {
        <?php if ($_smarty_tpl->tpl_vars['selected_color']->value) {?>
         color: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_color']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['selected_bg']->value) {?>
         background-color: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_bg']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
         <?php }?>
     }
     <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['header_font']->value||$_smarty_tpl->tpl_vars['header_size']->value||$_smarty_tpl->tpl_vars['header_mobile']->value) {?>
         .remodal-av h3 {
             <?php if ($_smarty_tpl->tpl_vars['header_font']->value) {?>
             font-family: "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['header_font']->value, ENT_QUOTES, 'UTF-8');?>
" !important;
            <?php }?>
             <?php if ($_smarty_tpl->tpl_vars['header_size']->value) {?>
             font-size: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['header_size']->value, ENT_QUOTES, 'UTF-8');?>
px;
             <?php }?>
         }

         <?php if ($_smarty_tpl->tpl_vars['header_size_mobile']->value) {?>
            @media(max-width: 768px) {
                .remodal-av h3 {
                    font-size: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['header_size_mobile']->value, ENT_QUOTES, 'UTF-8');?>
px;
                }
            }
         <?php }?>
     <?php }?>

     <?php if ($_smarty_tpl->tpl_vars['content_font']->value||$_smarty_tpl->tpl_vars['content_size']->value||$_smarty_tpl->tpl_vars['content_mobile']->value) {?>
         .remodal-av p {
             <?php if ($_smarty_tpl->tpl_vars['content_font']->value) {?>
                 font-family: "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['content_font']->value, ENT_QUOTES, 'UTF-8');?>
" !important;
             <?php }?>
             <?php if ($_smarty_tpl->tpl_vars['content_size']->value) {?>
                 font-size: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['content_size']->value, ENT_QUOTES, 'UTF-8');?>
px;
             <?php }?>
         }

         <?php if ($_smarty_tpl->tpl_vars['content_size_mobile']->value) {?>
             @media(max-width: 768px) {
                 .remodal-av p {
                     font-size: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['content_size_mobile']->value, ENT_QUOTES, 'UTF-8');?>
px;
                 }
             }
         <?php }?>
     <?php }?>
 </style>

<?php if ($_smarty_tpl->tpl_vars['ps_version']->value!="1.6") {?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js" type="text/javascript"></script>
<?php }?>

<script>
    var mode = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['mode']->value, ENT_QUOTES, 'UTF-8');?>
";

    function avLiveValidation()
    {
        $('.av-select').on('change', function() {
            performValidation();
        });
    }

    function avClassicValidation()
    {
        $('.remodal-confirm').click(function() {
            performValidation();
        });
    }

    function performValidation()
    {
        // Current date
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();

        // Selected date
        var day = $(".av-day").find("option:selected").val();
        var month = $(".av-month").find("option:selected").val();
        var year = $(".av-year").find("option:selected").val();

        if(year < yyyy-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['age']->value, ENT_QUOTES, 'UTF-8');?>
) {
            // Allowed
            avAllow();
        } else {
            if(year == yyyy-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['age']->value, ENT_QUOTES, 'UTF-8');?>
) {
                // Check month and day
                if(month < mm) {
                    // Allowed
                    avAllow();
                } else {
                    if(month == mm) {
                        if(day <= dd) {
                            // Allowed
                            avAllow();
                        } else {
                            // Not allowed
                            avReject();
                        }
                    } else {
                        // Not allowed
                        avReject();
                    }
                }
            } else {
                // Not allowed
                avReject();
            }
        }
    }

    jQuery( document ).ready(function() {
        <?php if ($_smarty_tpl->tpl_vars['mode']->value=="classic") {?>
        avClassicValidation();
        <?php } else { ?>
        avLiveValidation();
        <?php }?>

        jQuery(".remodal-confirm").click(function () {
            if($(this).attr("data-remodal-action") == "confirm") {
                avAjax();
            }
        });
    });
</script>

<div class="remodal remodal-av" data-remodal-id="modalav">

    <?php if ($_smarty_tpl->tpl_vars['title']->value) {?>
        <h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>
</h3>
    <?php } else { ?>
        <h3><?php echo smartyTranslate(array('s'=>'Age verification','mod'=>'ageverification'),$_smarty_tpl);?>
</h3>
    <?php }?>
    <p>
        <?php if ($_smarty_tpl->tpl_vars['text']->value) {?>
            <?php echo htmlspecialchars(nl2br($_smarty_tpl->tpl_vars['text']->value), ENT_QUOTES, 'UTF-8');?>

        <?php } else { ?>
            <?php echo smartyTranslate(array('s'=>'If you want to use our webiste you have to be at least','mod'=>'ageverification'),$_smarty_tpl);?>

            <strong><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['age']->value, ENT_QUOTES, 'UTF-8');?>
</strong> <?php echo smartyTranslate(array('s'=>'years old.','mod'=>'ageverification'),$_smarty_tpl);?>

        <?php }?>
    </p>

    <?php if ($_smarty_tpl->tpl_vars['type']->value=="date") {?>
        <select class="selectpicker av-select av-day">
            <?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i']->value = 1;
  if ($_smarty_tpl->tpl_vars['i']->value<=31) { for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value<=31; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['i']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['i']->value, ENT_QUOTES, 'UTF-8');?>
</option>
            <?php }} ?>
        </select>
        <select class="selectpicker av-select av-month">
            <option value="1"><?php echo smartyTranslate(array('s'=>'January','mod'=>'ageverification'),$_smarty_tpl);?>
</option>
            <option value="2"><?php echo smartyTranslate(array('s'=>'February','mod'=>'ageverification'),$_smarty_tpl);?>
</option>
            <option value="3"><?php echo smartyTranslate(array('s'=>'March','mod'=>'ageverification'),$_smarty_tpl);?>
</option>
            <option value="4"><?php echo smartyTranslate(array('s'=>'April','mod'=>'ageverification'),$_smarty_tpl);?>
</option>
            <option value="5"><?php echo smartyTranslate(array('s'=>'May','mod'=>'ageverification'),$_smarty_tpl);?>
</option>
            <option value="6"><?php echo smartyTranslate(array('s'=>'June','mod'=>'ageverification'),$_smarty_tpl);?>
</option>
            <option value="7"><?php echo smartyTranslate(array('s'=>'July','mod'=>'ageverification'),$_smarty_tpl);?>
</option>
            <option value="8"><?php echo smartyTranslate(array('s'=>'April','mod'=>'ageverification'),$_smarty_tpl);?>
</option>
            <option value="9"><?php echo smartyTranslate(array('s'=>'September','mod'=>'ageverification'),$_smarty_tpl);?>
</option>
            <option value="10"><?php echo smartyTranslate(array('s'=>'October','mod'=>'ageverification'),$_smarty_tpl);?>
</option>
            <option value="11"><?php echo smartyTranslate(array('s'=>'November','mod'=>'ageverification'),$_smarty_tpl);?>
</option>
            <option value="12"><?php echo smartyTranslate(array('s'=>'December','mod'=>'ageverification'),$_smarty_tpl);?>
</option>
        </select>
        <select class="selectpicker av-select av-year">
            <?php ob_start();?><?php echo htmlspecialchars(date('Y'), ENT_QUOTES, 'UTF-8');?>
<?php $_tmp1=ob_get_clean();?><?php ob_start();?><?php echo htmlspecialchars(date('Y'), ENT_QUOTES, 'UTF-8');?>
<?php $_tmp2=ob_get_clean();?><?php ob_start();?><?php echo htmlspecialchars($_tmp2-100, ENT_QUOTES, 'UTF-8');?>
<?php $_tmp3=ob_get_clean();?><?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i']->value = $_tmp1;
  if ($_smarty_tpl->tpl_vars['i']->value>=$_tmp3) { for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value>=$_tmp3; $_smarty_tpl->tpl_vars['i']->value--) {
?>
                <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['i']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['i']->value, ENT_QUOTES, 'UTF-8');?>
</option>
            <?php }} ?>
        </select>
    <?php }?>
    <br>

    <?php if ($_smarty_tpl->tpl_vars['mode']->value=="classic") {?>
        <p class="text-age" style="margin: 0 20px;display: none;color: red;margin-top:20px;">
            <?php if (!$_smarty_tpl->tpl_vars['age_text']->value) {?>
                <?php echo smartyTranslate(array('s'=>'Your age is too low','mod'=>'ageverification'),$_smarty_tpl);?>

            <?php } else { ?>
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['age_text']->value, ENT_QUOTES, 'UTF-8');?>

            <?php }?>
        </p>
    <?php }?>

    <button <?php if ($_smarty_tpl->tpl_vars['type']->value!="date") {?>style="margin-top: 0;"<?php }?>
            class="remodal-confirm dis"
            <?php if ($_smarty_tpl->tpl_vars['type']->value!="date") {?>data-remodal-action="confirm"<?php }?>>
        <span class="text-verified" style="display: none;margin: 0 20px;">
        <?php if (!$_smarty_tpl->tpl_vars['button_text']->value) {?>
            <?php echo smartyTranslate(array('s'=>'OK','mod'=>'ageverification'),$_smarty_tpl);?>

        <?php } else { ?>
            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['button_text']->value, ENT_QUOTES, 'UTF-8');?>

        <?php }?>
        </span>

        <span class="text-unverified" style="margin: 0 20px;">

            <?php if ($_smarty_tpl->tpl_vars['type']->value=="accept") {?>
                <?php if (!$_smarty_tpl->tpl_vars['button_text']->value) {?>
                    <?php echo smartyTranslate(array('s'=>'OK','mod'=>'ageverification'),$_smarty_tpl);?>

                <?php } else { ?>
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['button_text']->value, ENT_QUOTES, 'UTF-8');?>

                <?php }?>
            <?php } else { ?>
                <?php if ($_smarty_tpl->tpl_vars['mode']->value=="classic") {?>
                    <?php if (!$_smarty_tpl->tpl_vars['button_text']->value) {?>
                        <?php echo smartyTranslate(array('s'=>'Confirm','mod'=>'ageverification'),$_smarty_tpl);?>

                    <?php } else { ?>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['button_text']->value, ENT_QUOTES, 'UTF-8');?>

                    <?php }?>
                <?php } else { ?>
                    <?php if (!$_smarty_tpl->tpl_vars['button_text']->value) {?>
                        <?php echo smartyTranslate(array('s'=>'Please select your birthdate','mod'=>'ageverification'),$_smarty_tpl);?>

                    <?php } else { ?>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['button_text']->value, ENT_QUOTES, 'UTF-8');?>

                    <?php }?>
                <?php }?>

            <?php }?>
        </span>

        <?php if ($_smarty_tpl->tpl_vars['mode']->value=="classic") {?>
        <span class="text-age" style="margin: 0 20px;display: none;">
            <?php if (!$_smarty_tpl->tpl_vars['button_text']->value) {?>
                <?php echo smartyTranslate(array('s'=>'Confirm','mod'=>'ageverification'),$_smarty_tpl);?>

            <?php } else { ?>
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['button_text']->value, ENT_QUOTES, 'UTF-8');?>

            <?php }?>
        </span>
        <?php } else { ?>
        <span class="text-age" style="margin: 0 20px;display: none;">
             <?php if (!$_smarty_tpl->tpl_vars['age_text']->value) {?>
                 <?php echo smartyTranslate(array('s'=>'Your age is too low','mod'=>'ageverification'),$_smarty_tpl);?>

             <?php } else { ?>
                 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['age_text']->value, ENT_QUOTES, 'UTF-8');?>

             <?php }?>
        </span>
        <?php }?>
    </button>

</div>
<?php }} ?>
