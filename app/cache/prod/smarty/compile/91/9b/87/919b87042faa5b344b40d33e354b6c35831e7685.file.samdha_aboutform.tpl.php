<?php /* Smarty version Smarty-3.1.19, created on 2018-05-18 17:44:01
         compiled from "C:\xampp\htdocs\sexilima\modules\cron\\views\templates\admin\samdha_aboutform.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12174746255aff5731973c29-37185718%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '919b87042faa5b344b40d33e354b6c35831e7685' => 
    array (
      0 => 'C:\\xampp\\htdocs\\sexilima\\modules\\cron\\\\views\\templates\\admin\\samdha_aboutform.tpl',
      1 => 1526683430,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12174746255aff5731973c29-37185718',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'module_path' => 0,
    'module_display_name' => 0,
    'description_big_html' => 0,
    'description' => 0,
    'home_url' => 0,
    'module_version' => 0,
    'contact_url' => 0,
    'version_14' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5aff57319ba137_24736467',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5aff57319ba137_24736467')) {function content_5aff57319ba137_24736467($_smarty_tpl) {?>
<p style="font-size: 1.5em; font-weight: bold; padding-bottom: 0"><img src="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_path']->value,'htmlall','UTF-8');?>
logo.png" alt="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_display_name']->value,'htmlall','UTF-8');?>
" style="float: left; padding-right: 1em"/><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_display_name']->value,'htmlall','UTF-8');?>
</p>
<p style="clear: left;"><?php echo smartyTranslate(array('s'=>'Thanks for installing this module on your website.','mod'=>'samdha'),$_smarty_tpl);?>
</p>
<?php if ($_smarty_tpl->tpl_vars['description_big_html']->value) {?><?php echo $_smarty_tpl->tpl_vars['description_big_html']->value;?>
<?php } else { ?><p><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['description']->value,'htmlall','UTF-8');?>
</p><?php }?>
<p>
	<?php echo smartyTranslate(array('s'=>'Made with ❤ by','mod'=>'samdha'),$_smarty_tpl);?>
 <a style="color: #7ba45b; text-decoration: underline;" href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['home_url']->value,'htmlall','UTF-8');?>
">Samdha</a><?php echo smartyTranslate(array('s'=>', which helps you develop your e-commerce site.','mod'=>'samdha'),$_smarty_tpl);?>

</p>
<p>
	<span style="float: right; opacity: .2; font-size: 9px; padding-top: 5px;"><abbr title="<?php echo smartyTranslate(array('s'=>'version','mod'=>'samdha'),$_smarty_tpl);?>
">v</abbr><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_version']->value,'htmlall','UTF-8');?>
</span>
	<a href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['contact_url']->value,'htmlall','UTF-8');?>
" id="samdha_contact"><?php echo smartyTranslate(array('s'=>'Contact','mod'=>'samdha'),$_smarty_tpl);?>
</a>
</p>

<?php if (!$_smarty_tpl->tpl_vars['version_14']->value) {?>
	
    <style type="text/css">
        #content .warn {
            border: 1px solid #D3C200;
            background-color: #FFFAC6;
            font-family: Arial,Verdana,Helvetica,sans-serif;
        }
        #content .conf, #content .warn, #content .error {
            color: #383838;
            font-weight: 700;
            margin: 0 0 10px 0;
            line-height: 20px;
            padding: 10px 15px;
        }
    </style>
	
<?php }?>
<?php }} ?>
