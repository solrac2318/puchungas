<?php /* Smarty version Smarty-3.1.19, created on 2018-05-18 17:44:01
         compiled from "C:\xampp\htdocs\sexilima\modules\cron\\views\templates\admin\samdha_licenceform.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18471558665aff57319f8949-52321099%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0d9e6302c2c0cba866bfad6746496fc4502bca2d' => 
    array (
      0 => 'C:\\xampp\\htdocs\\sexilima\\modules\\cron\\\\views\\templates\\admin\\samdha_licenceform.tpl',
      1 => 1526683430,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18471558665aff57319f8949-52321099',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'registered' => 0,
    'content_html' => 0,
    'bootstrap' => 0,
    'space' => 0,
    'licence_url' => 0,
    'module_url' => 0,
    'licence_number' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5aff5731a2b5d3_79907768',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5aff5731a2b5d3_79907768')) {function content_5aff5731a2b5d3_79907768($_smarty_tpl) {?>
<?php if ($_smarty_tpl->tpl_vars['registered']->value) {?>
    <?php if ($_smarty_tpl->tpl_vars['content_html']->value) {?>
        <fieldset class="<?php if (!$_smarty_tpl->tpl_vars['bootstrap']->value) {?>ui-widget ui-widget-content ui-corner-all<?php }?><?php if ($_smarty_tpl->tpl_vars['space']->value) {?> space<?php }?>"><?php echo $_smarty_tpl->tpl_vars['content_html']->value;?>
</fieldset>
    <?php }?>
<?php } else { ?>
    <fieldset class="<?php if (!$_smarty_tpl->tpl_vars['bootstrap']->value) {?>ui-widget ui-widget-content ui-corner-all<?php }?><?php if ($_smarty_tpl->tpl_vars['space']->value) {?> space<?php }?>" id="samdha_registerform">
        <legend class="<?php if (!$_smarty_tpl->tpl_vars['bootstrap']->value) {?>ui-widget-header ui-corner-all<?php }?>"><?php echo smartyTranslate(array('s'=>'Register this module','mod'=>'samdha'),$_smarty_tpl);?>
</legend>
        <p><?php echo smartyTranslate(array('s'=>'By register your module you will get:','mod'=>'samdha'),$_smarty_tpl);?>
</p>
        <ul>
            <li><?php echo smartyTranslate(array('s'=>'Faster and better support,','mod'=>'samdha'),$_smarty_tpl);?>
</li>
            <li><?php echo smartyTranslate(array('s'=>'Latest version before everyone,','mod'=>'samdha'),$_smarty_tpl);?>
</li>
            <li><?php echo smartyTranslate(array('s'=>'Automatic update of the module.','mod'=>'samdha'),$_smarty_tpl);?>
</li>
        </ul>
        <p>
            <?php echo smartyTranslate(array('s'=>'Just fill','mod'=>'samdha'),$_smarty_tpl);?>
 <a style="text-decoration: underline;" href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['licence_url']->value,'htmlall','UTF-8');?>
" target="_blank" class="module_support"><?php echo smartyTranslate(array('s'=>'this form','mod'=>'samdha'),$_smarty_tpl);?>
</a><?php echo smartyTranslate(array('s'=>', it\'s free.','mod'=>'samdha'),$_smarty_tpl);?>

        </p>
        <hr/>
        <form action="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_url']->value,'htmlall','UTF-8');?>
" method="post" enctype="multipart/form-data">
            <p style="font-size: 0.85em;">
                <label for="licence_number" class="t"><?php echo smartyTranslate(array('s'=>'Licence number:','mod'=>'samdha'),$_smarty_tpl);?>
</label><br/>
                <input style="display: inline-block; width: 200px" type="text" name="licence_number" id="licence_number" value="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['licence_number']->value,'htmlall','UTF-8');?>
"/>
                <button class="ui-button-primary" name="saveLicence" value="1" style="margin-top: -.1em;"><?php echo smartyTranslate(array('s'=>'Ok','mod'=>'samdha'),$_smarty_tpl);?>
</button>
            </p>
        </form>
    </fieldset>
<?php }?>
<?php }} ?>
