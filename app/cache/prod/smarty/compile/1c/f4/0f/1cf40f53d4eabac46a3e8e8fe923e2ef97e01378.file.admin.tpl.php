<?php /* Smarty version Smarty-3.1.19, created on 2018-05-18 17:44:01
         compiled from "C:\xampp\htdocs\sexilima\modules\cron\views\templates\hook\admin.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13143756945aff5731bf0830-32112862%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1cf40f53d4eabac46a3e8e8fe923e2ef97e01378' => 
    array (
      0 => 'C:\\xampp\\htdocs\\sexilima\\modules\\cron\\views\\templates\\hook\\admin.tpl',
      1 => 1526683430,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13143756945aff5731bf0830-32112862',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'version_16' => 0,
    'bootstrap' => 0,
    'php_dir' => 0,
    'cron_command' => 0,
    'cron_url' => 0,
    'module_url' => 0,
    'module_short_name' => 0,
    'methods' => 0,
    'module_config' => 0,
    'crons' => 0,
    'crons_url' => 0,
    'cron' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5aff5731e230c2_72991443',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5aff5731e230c2_72991443')) {function content_5aff5731e230c2_72991443($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include 'C:\\xampp\\htdocs\\sexilima\\vendor\\prestashop\\smarty\\plugins\\function.html_options.php';
if (!is_callable('smarty_modifier_date_format')) include 'C:\\xampp\\htdocs\\sexilima\\vendor\\prestashop\\smarty\\plugins\\modifier.date_format.php';
?>
<div id="tabParameters" class="col-lg-10 col-md-9">
    <div class="panel">
        <h3 class="tab"> <i class="icon-wrench"></i> <?php echo smartyTranslate(array('s'=>'Parameters','mod'=>'cron'),$_smarty_tpl);?>
</h3>

        <div class="<?php if ($_smarty_tpl->tpl_vars['version_16']->value&&$_smarty_tpl->tpl_vars['bootstrap']->value) {?>alert alert-info<?php } else { ?>hint solid_hint<?php }?>">
            <?php echo smartyTranslate(array('s'=>'Please choose the method used to determine when executing jobs.','mod'=>'cron'),$_smarty_tpl);?>

            <ol style="margin-top: 1em">
            	<li style="list-style:decimal">
                    <?php echo smartyTranslate(array('s'=>'"Shop traffic" method doesn\'t need configuration but is not sure. It depends of your website frequentation so when it isn\'t visited, jobs are not executed.','mod'=>'cron'),$_smarty_tpl);?>

                </li>
                <li style="list-style:decimal">
                    <?php echo smartyTranslate(array('s'=>'"Server crontab" is the best method but only if your server uses Linux and you have access to crontab. In that case add the line below to your crontab file.','mod'=>'cron'),$_smarty_tpl);?>

                    <br/><pre>* * * * * <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['php_dir']->value,'htmlall','UTF-8');?>
 <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['cron_command']->value,'htmlall','UTF-8');?>
</pre>
                    <p>"<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['php_dir']->value,'htmlall','UTF-8');?>
" <?php echo smartyTranslate(array('s'=>'is the PHP path on your server. It has been automatically detected and may be wrong. If it doesn\'t work, contact your host.','mod'=>'cron'),$_smarty_tpl);?>
</p>
                </li>
                <li style="list-style:decimal">
                    <?php echo smartyTranslate(array('s'=>'"Webcron service" is a good alternative to crontab but is often not free. Register to a webcron service and configure it to visit the URL below every minutes or the nearest.','mod'=>'cron'),$_smarty_tpl);?>

                    <br/><pre><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['cron_url']->value,'htmlall','UTF-8');?>
</pre><br/>
             		<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAWCAYAAADEtGw7AAAErklEQVQ4T6WUeUyTZxzHv+/bgx4cpbRccogtggULDFzGlITDRUycZMumuENlcyM7Y+YfMrNljUt2uLCZLDrj/nAJcyNhKnMhuuzQxTjnOMYAC6VQFMaxcbWF0tLjffd7CzgQNEt80qdp0t/zeb7P9/d9HgZ3GadMkG3TIVLGI0osgprhIQMDMc/Axfng9Iox0WrHeNErmCEEdyeGWYHLXjoGxcNRWA0e6SKWLbnRrSyRKDVqsBKW84xOeaanbhkz/DW0UesYh76nTsJ++TL8i1nLwAQN3aiFnhYVWvviD0qStsembq4AFHEAIwY8o4CjE10NR2b02pbPCXZu3IPO2N0YW6x8Cdi0F7Kq7UhiAyiy/qX/wLDzhAqRawB+lqZvThAjoi8p4JuFpfY1v0596SgfwJmWf2B+6HU4F1QvAQ99BY1ajDyLNfTo+h2fpjHqVVTnAlie5vwS+gmOlnES2o+BpW7/tC7B+kaAw49bj2NgwZLb4MJCiC++ihSRCE/Y+o3vppa9LGIkbhJHfRHTFOBCtQD10/TRTgEFui/WQ6f9oY7jA9WnW/FnhQme4MEWpJuehPTQHqxva2SO5GwqKGZS8oEQAssCYBbgQjGBeS9BZ8kSTwgwbof5p7qRtHWO3c23cH3Bjtvg70xQbHkARnO79DPjBkM2EvWAchaMkpotD8wpp8wFlRKUdxHYJQNGx2C52jaVaph81jKMKxn7YJ8/15xmIbdP5yOro0V6LDt3VS5iwwAVD0ZBjZN4aQrm0hBC5SV/faTWQU38exKWJoczNXPymfYhXMmpCDZQMGxuBK14AYbW3/Be3kb9VkRNA5FuMH6yTEFgJRUJbCEc/aQ4XkE2SIBJLczXBvrTslzPNXehcZkVVM66G5AoCsE2mzn247WPpEmhMFN0KZ4BIibOK6AYC3eNlxLYlQnrpTHoUm6e4MCdPH0VncuaJyz74xxU6RHItTSLPzKWbsmBJhRMyE1SNgTop4QTAjY5nUQL3p8CTCnQ2XBhItXo3O8M4GdNCYbnihalImgHRa7qbSSzLDb33NBUpz++SwmlFIyb7AgjD+jksFPT5CGUDBms357l1iR3f0LvR933VrQ/Whl8N4JjxSu9KR1GhsPzvQNrKtY+tpfyRQa7KBksiZHTtaYPP/ILOs43dKVlc2/aXbgWUwbBpNuP0TKwcFHqqqANZ1HS0yapXleaEM2ocinTWwF3ByBygHe3oPuCxafTuw9zPM6+fwo9pjpQh/8bK71uc9HbgCzWL3mpb1C5R1+cSjmmOf4NJSQX1uvddNvcHT7RzIHeaTRl7MDEYuiKVswXsKNnEKNiYeidzquXSkJD4xIiIZN64fdI4AxEQzr55WGxeOaLD2sweKfae4GDqh80gAtRHxwYcCdHx8UlISpGh7HBVnDeqUDSyIvGJjNsRaagBf/roV9yqqbz79hmVEUpUapQhKvUcDoc8Nst7qyCcgry3ceKHi8uP/TWgV937azIl8tliIiIgNfrh83y+1hBcZn2vsDx8fGa8vKdjRkZmas1Gi1stl6fQhG2o7JyX/19gYXFGo1mbWlp6ddarTa8trb28PDwcM29oMJ//wLNGcAmi6ehdgAAAABJRU5ErkJggg=="> <?php echo smartyTranslate(array('s'=>'Schedule it in one click with','mod'=>'cron'),$_smarty_tpl);?>
 <a style="text-decoration:underline" href="https://www.easycron.com/cron-job-scheduler?ref=10651&specifiedBy=1&specifiedValue=1&url=<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['cron_url']->value,'url','UTF-8');?>
" target="_blank">EasyCron</a> <?php echo smartyTranslate(array('s'=>'(it\'s free)','mod'=>'cron'),$_smarty_tpl);?>
<br/><br/>
                </li>
            </ol>
        </div>

    	<form action="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_url']->value,'htmlall','UTF-8');?>
" method="post" enctype="multipart/form-data">
            <div class="form-group clear">
                <label for="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_method"><?php echo smartyTranslate(array('s'=>'Method','mod'=>'cron'),$_smarty_tpl);?>
</label>
                <div class="<?php if ($_smarty_tpl->tpl_vars['version_16']->value&&$_smarty_tpl->tpl_vars['bootstrap']->value) {?>input-group<?php } else { ?>margin-form<?php }?>">
                    <select name="setting[_method]" id="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_method">
                        <?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['methods']->value,'selected'=>$_smarty_tpl->tpl_vars['module_config']->value['_method']),$_smarty_tpl);?>

                    </select>
                </div>
            </div>

            <div class="form-group clear">
                <label><?php echo smartyTranslate(array('s'=>'Test job','mod'=>'cron'),$_smarty_tpl);?>
</label>
                <div class="margin-form">
                    <span class="radio">
                        <input type="radio" name="setting[_test]" id="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_test_on" value="1" <?php if ($_smarty_tpl->tpl_vars['module_config']->value['_test']) {?>checked="checked"<?php }?> />
                        <label for="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_test_on"><?php echo smartyTranslate(array('s'=>'Yes','mod'=>'cron'),$_smarty_tpl);?>
</label>
                        <input type="radio" name="setting[_test]" id="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_test_off" value="0" <?php if (!$_smarty_tpl->tpl_vars['module_config']->value['_test']) {?>checked="checked"<?php }?> />
                        <label for="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_test_off"><?php echo smartyTranslate(array('s'=>'No','mod'=>'cron'),$_smarty_tpl);?>
</label>
                    </span>
                    <p <?php if ($_smarty_tpl->tpl_vars['version_16']->value) {?>class="help-block"<?php }?>>
                        <?php echo smartyTranslate(array('s'=>'To check whether the choosen method works, you can enable the test job. It should be executed every minutes and show it at the top of this form. When everything is ok you can disable it.','mod'=>'cron'),$_smarty_tpl);?>

                    </p>
                </div>
            </div>

            <div class="clear panel-footer">
            	<input type="hidden" name="active_tab" value="tabParameters" />
                <input type="hidden" name="saveSettings" value="1">
				<p>
					<input type="submit" class="samdha_button" value="<?php echo smartyTranslate(array('s'=>'Save','mod'=>'cron'),$_smarty_tpl);?>
" />
				</p>
	        </div>
    	</form>
    </div>
</div>

<div id="tabJobs" class="col-lg-10 col-md-9">
    <div class="panel">
        <h3 class="tab"> <i class="icon-list"></i> <?php echo smartyTranslate(array('s'=>'Jobs','mod'=>'cron'),$_smarty_tpl);?>
</h3>

        <?php if (count($_smarty_tpl->tpl_vars['crons']->value)||count($_smarty_tpl->tpl_vars['crons_url']->value)) {?>
            <fieldset>
                <legend><?php echo smartyTranslate(array('s'=>'Crons jobs','mod'=>'cron'),$_smarty_tpl);?>
</legend>
                <table class="table">
                    <?php if (count($_smarty_tpl->tpl_vars['crons']->value)) {?>
                        <tr>
                            <th><?php echo smartyTranslate(array('s'=>'Module','mod'=>'cron'),$_smarty_tpl);?>
</th>
                            <th><?php echo smartyTranslate(array('s'=>'Method','mod'=>'cron'),$_smarty_tpl);?>
</th>
                            <th><?php echo smartyTranslate(array('s'=>'Schedule','mod'=>'cron'),$_smarty_tpl);?>
</th>
                            <th><?php echo smartyTranslate(array('s'=>'Last execution','mod'=>'cron'),$_smarty_tpl);?>
</th>
                            <th><?php echo smartyTranslate(array('s'=>'Action','mod'=>'cron'),$_smarty_tpl);?>
</th>
                        </tr>

                        <?php  $_smarty_tpl->tpl_vars['cron'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cron']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['crons']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cron']->key => $_smarty_tpl->tpl_vars['cron']->value) {
$_smarty_tpl->tpl_vars['cron']->_loop = true;
?>
                            <tr>
                                <td><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['cron']->value['name'],'htmlall','UTF-8');?>
</td>
                                <td><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['cron']->value['method'],'htmlall','UTF-8');?>
</td>
                                <td><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['cron']->value['mhdmd'],'htmlall','UTF-8');?>
</td>
                                <td>
                                    <?php if ($_smarty_tpl->tpl_vars['cron']->value['last_execution']) {?>
                                        <?php $_smarty_tpl->_capture_stack[0][] = array('temp', null, null); ob_start(); ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape(smarty_modifier_date_format($_smarty_tpl->tpl_vars['cron']->value['last_execution'],"%Y-%m-%d %H:%M:%S"),'htmlall','UTF-8');?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
                                        <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['dateFormat'][0][0]->dateFormat(array('date'=>Smarty::$_smarty_vars['capture']['temp'],'full'=>1),$_smarty_tpl);?>

                                    <?php } else { ?>
                                        <?php echo smartyTranslate(array('s'=>'Never','mod'=>'cron'),$_smarty_tpl);?>

                                    <?php }?>
                                </td>
                                <td>
                                    <a href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_url']->value,'htmlall','UTF-8');?>
&amp;delete=<?php echo intval($_smarty_tpl->tpl_vars['cron']->value['id_cron']);?>
">
                                        <img src="../img/admin/delete.gif" alt="<?php echo smartyTranslate(array('s'=>'Delete','mod'=>'cron'),$_smarty_tpl);?>
" title="<?php echo smartyTranslate(array('s'=>'Delete','mod'=>'cron'),$_smarty_tpl);?>
" />
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php }?>
                    <?php if (count($_smarty_tpl->tpl_vars['crons_url']->value)) {?>
                        <tr>
                            <th colspan="2"><?php echo smartyTranslate(array('s'=>'Url','mod'=>'cron'),$_smarty_tpl);?>
</th>
                            <th><?php echo smartyTranslate(array('s'=>'Schedule','mod'=>'cron'),$_smarty_tpl);?>
</th>
                            <th><?php echo smartyTranslate(array('s'=>'Last execution','mod'=>'cron'),$_smarty_tpl);?>
</th>
                            <th><?php echo smartyTranslate(array('s'=>'Action','mod'=>'cron'),$_smarty_tpl);?>
</th>
                        </tr>
                        <?php  $_smarty_tpl->tpl_vars['cron'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cron']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['crons_url']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cron']->key => $_smarty_tpl->tpl_vars['cron']->value) {
$_smarty_tpl->tpl_vars['cron']->_loop = true;
?>
                            <tr>
                                <td colspan="2"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['cron']->value['url'],'htmlall','UTF-8');?>
</td>
                                <td><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['cron']->value['mhdmd'],'htmlall','UTF-8');?>
</td>
                                <td>
                                    <?php if ($_smarty_tpl->tpl_vars['cron']->value['last_execution']) {?>
                                        <?php $_smarty_tpl->_capture_stack[0][] = array('temp', null, null); ob_start(); ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape(smarty_modifier_date_format($_smarty_tpl->tpl_vars['cron']->value['last_execution'],"%Y-%m-%d %H:%M:%S"),'htmlall','UTF-8');?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
                                        <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['dateFormat'][0][0]->dateFormat(array('date'=>Smarty::$_smarty_vars['capture']['temp'],'full'=>1),$_smarty_tpl);?>

                                    <?php } else { ?>
                                        <?php echo smartyTranslate(array('s'=>'Never','mod'=>'cron'),$_smarty_tpl);?>

                                    <?php }?>
                                </td>
                                <td>
                                    <a href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_url']->value,'htmlall','UTF-8');?>
&amp;delete_url=<?php echo intval($_smarty_tpl->tpl_vars['cron']->value['id_cron_url']);?>
">
                                        <img src="../img/admin/delete.gif" alt="<?php echo smartyTranslate(array('s'=>'Delete','mod'=>'cron'),$_smarty_tpl);?>
" title="<?php echo smartyTranslate(array('s'=>'Delete','mod'=>'cron'),$_smarty_tpl);?>
" />
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php }?>
                </table>
            </fieldset>
        <?php }?>

        <form action="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_url']->value,'htmlall','UTF-8');?>
" method="post" id="cron_add">
            <fieldset class="space width3">
                <legend><?php echo smartyTranslate(array('s'=>'Add a job','mod'=>'cron'),$_smarty_tpl);?>
</legend>
                <div class="<?php if ($_smarty_tpl->tpl_vars['version_16']->value&&$_smarty_tpl->tpl_vars['bootstrap']->value) {?>alert alert-info<?php } else { ?>hint solid_hint<?php }?>">
                    <?php echo smartyTranslate(array('s'=>'The URL below will be visited at the time or date specified.','mod'=>'cron'),$_smarty_tpl);?>

                </div>

                <div class="form-group clear">
                    <label for="cron_method"><?php echo smartyTranslate(array('s'=>'URL','mod'=>'cron'),$_smarty_tpl);?>
</label>
                    <div class="margin-form">
                        <input required="required" type="url" name="cron_url" id="cron_url" value="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape((($tmp = @$_POST['cron_url'])===null||$tmp==='' ? '' : $tmp),'htmlall','UTF-8');?>
" />
                    </div>
                </div>

                <div class="form-group clear">
                    <label for="cron_method"><?php echo smartyTranslate(array('s'=>'Schedule','mod'=>'cron'),$_smarty_tpl);?>
</label>
                    <div class="margin-form">
                        <input type="hidden" name="cron_mhdmd" id="cron_mhdmd" value="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape((($tmp = @$_POST['cron_mhdmd'])===null||$tmp==='' ? '' : $tmp),'htmlall','UTF-8');?>
" />
                        <select name="cron_mhdmd2" id="cron_mhdmd2">
                            <option value="0 * * * *"><?php echo smartyTranslate(array('s'=>'Every hours','mod'=>'cron'),$_smarty_tpl);?>
</option>
                            <option value="0 0 * * *" selected="selected"><?php echo smartyTranslate(array('s'=>'Daily (midnight)','mod'=>'cron'),$_smarty_tpl);?>
</option>
                            <option value="0 0 * * 0"><?php echo smartyTranslate(array('s'=>'Weekly (Sunday)','mod'=>'cron'),$_smarty_tpl);?>
</option>
                            <option value="0 0 1 * *"><?php echo smartyTranslate(array('s'=>'Monthly (first)','mod'=>'cron'),$_smarty_tpl);?>
</option>
                            <option value="0 0 1 1 *" ><?php echo smartyTranslate(array('s'=>'Yearly (January 1)','mod'=>'cron'),$_smarty_tpl);?>
</option>
                            <option value="other" ><?php echo smartyTranslate(array('s'=>'Other','mod'=>'cron'),$_smarty_tpl);?>
</option>
                        </select>
                    </div>
                </div>

                <div id="cron_table" style="display: none">
                    <div class="form-group clear">
                        <label><?php echo smartyTranslate(array('s'=>'Minutes','mod'=>'cron'),$_smarty_tpl);?>
</label>
                        <div class="margin-form">
                            <span class="radio">
                                <input type="radio" name="all_mins" id="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_all_mins_on" value="1" checked="checked" />
                                <label for="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_all_mins_on"><?php echo smartyTranslate(array('s'=>'All','mod'=>'cron'),$_smarty_tpl);?>
</label>
                                <input type="radio" name="all_mins" id="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_all_mins_off" value="0" />
                                <label for="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_all_mins_off"><?php echo smartyTranslate(array('s'=>'Selected','mod'=>'cron'),$_smarty_tpl);?>
</label>
                            </span>
                            <div class="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_all_mins_off">
                                <select multiple="multiple" size="12" name="mins" class="nochosen">
                                    <?php echo smarty_function_html_options(array('options'=>range(0,59)),$_smarty_tpl);?>

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group clear">
                        <label><?php echo smartyTranslate(array('s'=>'Hours','mod'=>'cron'),$_smarty_tpl);?>
</label>
                        <div class="margin-form">
                            <span class="radio">
                                <input type="radio" name="all_hours" id="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_all_hours_on" value="1" checked="checked" />
                                <label for="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_all_hours_on"><?php echo smartyTranslate(array('s'=>'All','mod'=>'cron'),$_smarty_tpl);?>
</label>
                                <input type="radio" name="all_hours" id="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_all_hours_off" value="0" />
                                <label for="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_all_hours_off"><?php echo smartyTranslate(array('s'=>'Selected','mod'=>'cron'),$_smarty_tpl);?>
</label>
                            </span>
                            <div class="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_all_hours_off">
                                <select multiple="multiple" size="12" name="hours" class="nochosen">
                                    <?php echo smarty_function_html_options(array('options'=>range(0,23)),$_smarty_tpl);?>

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group clear">
                        <label><?php echo smartyTranslate(array('s'=>'Days of month','mod'=>'cron'),$_smarty_tpl);?>
</label>
                        <div class="margin-form">
                            <span class="radio">
                                <input type="radio" name="all_days" id="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_all_days_on" value="1" checked="checked" />
                                <label for="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_all_days_on"><?php echo smartyTranslate(array('s'=>'All','mod'=>'cron'),$_smarty_tpl);?>
</label>
                                <input type="radio" name="all_days" id="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_all_days_off" value="0" />
                                <label for="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_all_days_off"><?php echo smartyTranslate(array('s'=>'Selected','mod'=>'cron'),$_smarty_tpl);?>
</label>
                            </span>
                            <div class="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_all_days_off">
                                <select multiple="multiple" size="12" name="days" class="nochosen">
                                    <?php echo smarty_function_html_options(array('options'=>range(0,31)),$_smarty_tpl);?>

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group clear">
                        <label><?php echo smartyTranslate(array('s'=>'Months','mod'=>'cron'),$_smarty_tpl);?>
</label>
                        <div class="margin-form">
                            <span class="radio">
                                <input type="radio" name="all_months" id="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_all_months_on" value="1" checked="checked" />
                                <label for="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_all_months_on"><?php echo smartyTranslate(array('s'=>'All','mod'=>'cron'),$_smarty_tpl);?>
</label>
                                <input type="radio" name="all_months" id="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_all_months_off" value="0" />
                                <label for="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_all_months_off"><?php echo smartyTranslate(array('s'=>'Selected','mod'=>'cron'),$_smarty_tpl);?>
</label>
                            </span>
                            <div class="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_all_months_off">
                                <select multiple="multiple" size="12" name="months" class="nochosen">
                                    <option value="1" ><?php echo smartyTranslate(array('s'=>'January','mod'=>'cron'),$_smarty_tpl);?>
</option>
                                    <option value="2" ><?php echo smartyTranslate(array('s'=>'February','mod'=>'cron'),$_smarty_tpl);?>
</option>
                                    <option value="3" ><?php echo smartyTranslate(array('s'=>'March','mod'=>'cron'),$_smarty_tpl);?>
</option>
                                    <option value="4" ><?php echo smartyTranslate(array('s'=>'April','mod'=>'cron'),$_smarty_tpl);?>
</option>
                                    <option value="5" ><?php echo smartyTranslate(array('s'=>'May','mod'=>'cron'),$_smarty_tpl);?>
</option>
                                    <option value="6" ><?php echo smartyTranslate(array('s'=>'June','mod'=>'cron'),$_smarty_tpl);?>
</option>
                                    <option value="7" ><?php echo smartyTranslate(array('s'=>'July','mod'=>'cron'),$_smarty_tpl);?>
</option>
                                    <option value="8" ><?php echo smartyTranslate(array('s'=>'August','mod'=>'cron'),$_smarty_tpl);?>
</option>
                                    <option value="9" ><?php echo smartyTranslate(array('s'=>'September','mod'=>'cron'),$_smarty_tpl);?>
</option>
                                    <option value="10" ><?php echo smartyTranslate(array('s'=>'October','mod'=>'cron'),$_smarty_tpl);?>
</option>
                                    <option value="11" ><?php echo smartyTranslate(array('s'=>'November','mod'=>'cron'),$_smarty_tpl);?>
</option>
                                    <option value="12" ><?php echo smartyTranslate(array('s'=>'December','mod'=>'cron'),$_smarty_tpl);?>
</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group clear">
                        <label><?php echo smartyTranslate(array('s'=>'Days of week','mod'=>'cron'),$_smarty_tpl);?>
</label>
                        <div class="margin-form">
                            <span class="radio">
                                <input type="radio" name="all_weekdays" id="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_all_weekdays_on" value="1" checked="checked" />
                                <label for="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_all_weekdays_on"><?php echo smartyTranslate(array('s'=>'All','mod'=>'cron'),$_smarty_tpl);?>
</label>
                                <input type="radio" name="all_weekdays" id="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_all_weekdays_off" value="0" />
                                <label for="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_all_weekdays_off"><?php echo smartyTranslate(array('s'=>'Selected','mod'=>'cron'),$_smarty_tpl);?>
</label>
                            </span>
                            <div class="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_short_name']->value,'htmlall','UTF-8');?>
_all_weekdays_off">
                                <select multiple="multiple" size="12" name="weekdays" class="nochosen">
                                    <option value="0" ><?php echo smartyTranslate(array('s'=>'Sunday','mod'=>'cron'),$_smarty_tpl);?>
</option>
                                    <option value="1" ><?php echo smartyTranslate(array('s'=>'Monday','mod'=>'cron'),$_smarty_tpl);?>
</option>
                                    <option value="2" ><?php echo smartyTranslate(array('s'=>'Tuesday','mod'=>'cron'),$_smarty_tpl);?>
</option>
                                    <option value="3" ><?php echo smartyTranslate(array('s'=>'Wednesday','mod'=>'cron'),$_smarty_tpl);?>
</option>
                                    <option value="4" ><?php echo smartyTranslate(array('s'=>'Thursday','mod'=>'cron'),$_smarty_tpl);?>
</option>
                                    <option value="5" ><?php echo smartyTranslate(array('s'=>'Friday','mod'=>'cron'),$_smarty_tpl);?>
</option>
                                    <option value="6" ><?php echo smartyTranslate(array('s'=>'Saturday','mod'=>'cron'),$_smarty_tpl);?>
</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>

            <div class="clear panel-footer">
            	<input type="hidden" name="active_tab" value="tabJobs" />
                <input type="hidden" name="submitAddCron" value="1">
				<p>
					<input type="submit" class="samdha_button" value="<?php echo smartyTranslate(array('s'=>'Save','mod'=>'cron'),$_smarty_tpl);?>
" />
				</p>
	        </div>
        </form>
    </div>
</div>
<?php }} ?>
